class Admin::PrzelewController < Admin::BaseController

  def potwierdz
    payment = Payment.find(params[:format])
    payment.state = "completed"
    payment.save
    redirect_to admin_order_payments_path(payment.order)
  end

  def anuluj
    payment = Payment.find(params[:format])
    payment.state = "checkout"
    payment.save
    redirect_to admin_order_payments_path(payment.order)
  end

end