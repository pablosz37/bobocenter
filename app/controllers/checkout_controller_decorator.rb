CheckoutController.class_eval do

  #def before_address
   # @order.bill_address ||= Address.new(:country => default_country)
  #  @order.ship_address ||= Address.new(:country => default_country)
  
    #wyświetlanie adresu z ostatniego zamównienia
   # if session[:guest_token].blank?
    #  last_order = Order.find_by_sql("select o.* from orders o, users u where o.user_id = u.id and u.email = '#{@order.email}' and o.state = 'complete' order by o.created_at desc").first
     # if last_order
      #  if last_order.bill_address
       #   @order.bill_address = last_order.bill_address.clone
        #  @order.ship_address = last_order.ship_address.clone
      #  end
    #  end
    #end
    #end wyświetlanie
  #end
  
  def update_registration
    # hack - temporarily change the state to something other than cart so we can validate the order email address
    current_order.state = "address"
    current_order.bill_address = nil
    current_order.ship_address = nil
    if current_order.update_attributes(params[:order])
      redirect_to checkout_path
    else
      flash[:error] = t('guest_email_error')
      @user = User.new
      render 'registration'
    end
  end

  # dodano begin rescue
  def update
    if @order.update_attributes(object_params)
    begin
      if @order.next
        state_callback(:after)
      else
        flash[:error] = I18n.t(:payment_processing_failed)
        redirect_to checkout_state_path(@order.state) and return
      end
    rescue
      redirect_to cart_path and return
    end
      if @order.state == "complete" or @order.completed?
        flash[:notice] = I18n.t(:order_processed_successfully)
        flash[:commerce_tracking] = "nothing special"
        redirect_to completion_route
      else
        redirect_to checkout_state_path(@order.state)
      end

    else
      render :edit
    end
  end

end