OrdersController.class_eval do

  def edit
    @order = current_order(true)
    #dodano blokadę dla zerowych stanów magazynowych
    @order.line_items.each do |li|
      if li.quantity > li.variant.on_hand
        li.quantity = li.variant.on_hand
        flash[:error] = t("zero_on_hand_error")
      end
    end
    if @order.update_attributes(params[:order])
      @order.line_items = @order.line_items.select {|li| li.quantity > 0 }
    end
    @order.update_totals
    #koniec blokady
  end

end