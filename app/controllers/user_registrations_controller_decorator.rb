UserRegistrationsController.class_eval do

  def create
    @user = build_resource(params[:user])
    logger.debug(@user)
    if resource.save
      set_flash_message(:notice, :signed_up)
      sign_in_and_redirect(:user, @user)
    else
      #dodano flash
      flash[:error] = @user.errors.full_messages.first
      #end dodano
      clean_up_passwords(resource)
      render_with_scope(:new)
    end
  end

end