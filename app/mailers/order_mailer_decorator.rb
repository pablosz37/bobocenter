OrderMailer.class_eval do

  def confirm_email(order, resend=false)
    @order = order
    #subject = (resend ? "[RESEND] " : "")
    subject = "#{Spree::Config[:site_name]} Informacje o zamowieniu ##{order.number}"
    mail(:to => order.email,
         :subject => subject)
  end

  def cancel_email(order, resend=false)
    @order = order
    #subject = (resend ? "[RESEND] " : "")
    subject = "#{Spree::Config[:site_name]} Anulowano zamowienie ##{order.number}"
    mail(:to => order.email,
         :subject => subject)
  end
  
end
