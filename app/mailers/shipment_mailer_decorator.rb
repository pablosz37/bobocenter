ShipmentMailer.class_eval do

  def shipped_email(shipment, resend=false)
    @shipment = shipment
    #subject = (resend ? "[RESEND] " : "")
    subject = "#{Spree::Config[:site_name]} Informacje o wysylce ##{shipment.order.number}"
    mail(:to => shipment.order.email,
         :subject => subject)
  end
  
end