class Calculator::KurierDpd < Calculator
  preference :default_weight, :float, :default => 300
  
  def self.description
    "Przesylka kurierska - DPD"
  end

  def self.register
    super
    ShippingMethod.register_calculator(self)
  end

#opcje kiedy wysyłka ma być dostępna
  def available?(order)
    total_weight = compute_total_weight(order)
    if total_weight >= 500000
      return false
    else
      return true
    end
  end
  
  def compute(order)
    #gabaryt A
    total_weight = compute_total_weight(order)
    @tw = total_weight
    if total_weight < 3000 
      return 21.00
    elsif total_weight < 10000
      return 25.00
    elsif total_weight < 20000
      return 32.00
    elsif total_weight < 31500
      return 38.00
    elsif total_weight < 50000
      return 89.00
    elsif total_weight < 100000
      return 153.00
    elsif total_weight < 300000
      return 218.00
    elsif total_weight < 500000
      return 280.00
    end
  end
  
  def compute_total_weight(order)
    wrapper_weight = 200
    total_weight = wrapper_weight
    order.line_items.each do |item|
      total_weight += item.quantity * (item.variant.weight  || self.preferred_default_weight)
    end
    total_weight
  end
  
  def total_weight
    @tw  
  end
  
end
