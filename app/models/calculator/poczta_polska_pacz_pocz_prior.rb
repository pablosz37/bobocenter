class Calculator::PocztaPolskaPaczPoczPrior < Calculator
  preference :default_weight, :float, :default => 300
  
  def self.description
    "Cennik Poczty Polskiej - paczka pocztowa priorytetowa"
  end

  def self.register
    super
    ShippingMethod.register_calculator(self)
  end

#opcje kiedy wysyłka ma być dostępna
  def available?(order)
    return false if tylko_kurier(order) 
     total_weight = compute_total_weight(order)
     if total_weight >= 10000
       return false
     else
       return true
     end
  end
  
  def compute(order)
    #gabaryt A
    total_weight = compute_total_weight(order)
    @tw = total_weight
    if total_weight < 1000 
      return 11.00
    elsif total_weight < 2000
      return 13.00
    elsif total_weight < 5000
      return 14.50
    elsif total_weight < 10000
      return 20.50
    end
  end
  
  def tylko_kurier(order)
    order.line_items.each do |item|
      s_c = item.product.shipping_category
        if s_c
          return true if s_c.name == "Tylko kurier"
        end
    end
    false
  end
  
  def compute_total_weight(order)
    wrapper_weight = 200
    total_weight = wrapper_weight
    order.line_items.each do |item|
      total_weight += item.quantity * (item.variant.weight  || self.preferred_default_weight)
    end
    total_weight
  end
  
  def total_weight
    @tw  
  end
  
end
