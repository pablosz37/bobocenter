Image.class_eval do
  has_attached_file :attachment, 
                  :styles => { :mini => '48x48>', :small => '132x132>', :product => '308x308>', :large => '704x704>' },  
                  :default_style => :product,
                  :url => "/assets/products/:id/:style/:basename.:extension",
                  :path => ":rails_root/public/assets/products/:id/:style/:basename.:extension"
end