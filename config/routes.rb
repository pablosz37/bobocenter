Rails.application.routes.draw do
  # Add your extension routes here
  match 'admin/przelew/potwierdz' => 'admin/przelew#potwierdz', :as => :admin_przelew_potwierdz
  match 'admin/przelew/anuluj' => 'admin/przelew#anuluj', :as => :admin_przelew_anuluj
end
