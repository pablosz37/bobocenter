require 'spree_core'
require 'bobo_center_hooks'

module BoboCenter
  class Engine < Rails::Engine

    config.autoload_paths += %W(#{config.root}/lib)

    def self.activate
      Dir.glob(File.join(File.dirname(__FILE__), "../app/**/*_decorator*.rb")) do |c|
        Rails.env.production? ? require(c) : load(c)
      end
      Calculator::PocztaPolskaPrzesPolEko.register
      Calculator::PocztaPolskaPrzesPolPrior.register
      Calculator::PocztaPolskaPaczPoczEko.register
      Calculator::PocztaPolskaPaczPoczPrior.register
      Calculator::KurierDpd.register
      PaymentMethod::Przelew.register
    end

    config.to_prepare &method(:activate).to_proc
  end
end
