class BoboCenterHooks < Spree::ThemeSupport::HookListener
    
  replace :admin_dashboard_center do
      %(<div class="dashboard_main">
        <div class="dashboard_main_wrapper">
          <h2 id="order_by_day_title"><%= t('orders') %> <%= t('count') %> <%= t('by_day') %> (<%= t('last_7_days') %>)</h2>

          <table id="order_totals">
            <tr>
              <td style="width:23%;">
                <label id="orders_total"><%= number_with_delimiter @orders_total.to_i %> <%= t("number.currency.format.unit") %></label>
                <span><%= t('order') %> <%= t('total') %></span>
                
              </td>
              <td class="spacer">|</td>
              <td style="width:23%;">
                <label id="orders_line_total"><%= number_with_delimiter @orders_line_total.to_i %> <%= t("number.currency.format.unit") %></label>
                <span><%= t('item') %> <%= t('total') %></span>
              </td>
              <td class="spacer">|</td>
              <td style="width:23%;">
                <label id="orders_adjustment_total"><%= number_with_delimiter @orders_adjustment_total.to_i %> <%= t("number.currency.format.unit") %></label>
                <span><%= t('adjustments') %></span>
              </td>
              <td class="spacer">|</td>
              <td style="width:22%">
                <label id="orders_adjustment_total"><%= number_with_delimiter @orders_credit_total.to_i %> <%= t("number.currency.format.unit") %></label>
                <span><%= t('credits') %></span>
              </td>
            </tr>
          </table>

          <div id="orders_by_day" style="width:100%;height:240px;"></div>

          <div id="orders_by_day_options">
            <label><%= t('range') %>:</label>
            <%= select_tag "orders_by_day_reports", options_for_select([[t('last_7_days'), "7_days"], [t('last_14_days'), "14_days"], [t('this_month'), "this_month"],
                                                    [t('last_month'), "last_month"], [t('this_year'), "this_year"], [t('last_year'), "last_year"] ], "7_days") %>
            <label><%= t('value') %>:</label>
            <%= select_tag "orders_by_day_value", options_for_select({t('count') => 'Count', t('value') => 'Value'}, 'Count') %>
          </div>
        </div>
      </div>)
    end
    
  insert_after :admin_products_index_headers do
      %(<th><%= order @search, :by => :cost_price, :as => t("cost_price") %></th>
        <th><%= order @search, :by => :on_hand, :as => t("on_hand") %></th>)
  end
  
  insert_after :admin_products_index_rows do
      %(<td><%= product.master.cost_price %></td>
      <td><%= product.master.on_hand %></td>)
  end
  
  
end